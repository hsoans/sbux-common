## Semantic Logging Overview
### What is semantic?
The word "semantic" refers to logging of events in a "meaningful" (to programs and perhaps people) way. 
Normally, log messages are text that are filed away to be grepped and interpreted by users. 
With semantic logging (sometimes called "typed" logging), instead of just logging text, 
event objects which make sense in terms of the domain model are logged. This has the following advantages:

 * structuring the log information as event objects in the domain model forces the programmer to 
    think about the meaning of log event and to capture the meaning in source code
 * the event objects can be output as strings that can grepped, like normal logging
 * the event objects can be serialized to json which capture all the relevant information.
    The json can be streamed to a service that aggregates, stores and provides searching;
    For example, log events streamed to [logstash](https://www.elastic.co/products/logstash) 
    and searched with Elastic Search. Without slog / json encoder, logs have to be parsed back again
    into json with potential loss of information.
 * Whether log to text or json is just a change in configuration with the right encoder and appender
  
#### References of "Semantic / Strongly typed logging":  
(the following talk is in the context of .Net, but same principles apply to java/scala also..)

 - Video: [Creating Structured and Meaningful Logs with Semantic Logging](https://channel9.msdn.com/Events/Build/2013/3-336)
 - [Power Point Presentation](http://video.ch9.ms/sessions/build/2013/3-336.pptx)
 - Typed Events can be written as json directly into logstash, see for example 
  [Recommendations on Logging](http://kielczewski.eu/2014/12/few-recommendations-on-logging/), section: logging to json

## SLogger implementation
SLogger is a performant wrapper of slf4j, based on [scala-logging](https://github.com/typesafehub/scala-logging).
Lightbend scala-logging is used under the terms of Apache 2.0 license.
Like slf4j, each event is logged to a hierarchical context with a log level. For example:
```scala
  val LoyaltyContextName = "com.sbux.loyalty"
  val slogger = SLogger(LoyaltyContextName)
  slogger.info(CustomerCreatedEvent(when = ZonedDateTime.now, name = "Tigger", Tier.PREFERRED))
  
  //instead of naming the logging context, the trait SLogging can be used to mixin the logger to your class:
  package com.sbux.mydomain
  class MyClass extends BaseClass with SLogging {
    //slogger context is set to com.sbux.mydomain.MyClass
    slogger.warn(CustomerAnniverary(name = "Tigger", anniversaryDate))
  }
```
Like slf4j, logging of messages can be configured based on the context and level. 
For example, suppose com.sbux log level is set to WARN, then all messages logged below com.sbux context are
output only if they are WARN, ERROR or above level.

The slogger is performant, because the expression passed in tye info(...) call is evaluated _only if_ info level is
enabled. In other words, if info level is not enabled, then the performance penalty of constructing the event object
passed to the log method is not incurred. In java, this is usually achieved by enclosing the log calls in
```java
  if (logger.isInfoEnabled()) { log statement; }
```
## Status
SLogger is implemented to support logging of domain event objects that are serializable using toString (and json).
The trait SLogging can be used to mixin the logger into a class. Unit tests and akka tests are added.

### Further Work
The implementation is simple enough and additional features will be added based on user feedback.

* support "plain-logging" (without forcing domain events)
* To be verified: does our play micr0-service refresh logging configuration at runtime?
  For example, jboss container refreshes logging configuration on the fly if log4j.xml is changed..
* type-check whether json serialization exists for the domain event object
* use the json decoder / appender to send json log to logstash