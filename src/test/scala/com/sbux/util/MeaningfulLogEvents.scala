package com.sbux.util

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

// Enumeration of tiers
sealed abstract class Tier(val name: String)
object Tier {
  case object PREFERRED extends Tier("preferred")
  case object ORDINARY extends Tier("ordinary")
}

case class CustomerCreatedEvent(when: ZonedDateTime, name: String, tier: Tier) {
  override def toString: String =
    s"customer created on ${when.format(DateTimeFormatter.ISO_LOCAL_DATE)}, name: $name, tier: ${tier.name}"
}
