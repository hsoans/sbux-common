package com.sbux.util

import java.time.ZonedDateTime

import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.classic.{Logger => LogbackLogger}
import ch.qos.logback.core.Appender
import org.mockito.ArgumentMatcher
import org.mockito.ArgumentMatchers.argThat
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers, Suite}
import org.scalatest.mock.MockitoSugar.mock
import org.mockito.Mockito.{verify, when}


class SLoggerSpec extends Suite
  with FlatSpecLike
  with Matchers
  with BeforeAndAfterAll
{
  val slogger = SLogger(classOf[SLoggerSpec])

  "logger" should "log to output when corresponding log level is enabled" in {
    // get away with a type cast in unit test, knowing that underlying log implementation is logback
    val underlying: LogbackLogger = slogger.underlying.asInstanceOf[LogbackLogger]

    // we mock the logback appender to verify the log that is appended...
    val mockAppender = mock[Appender[ILoggingEvent]]
    underlying.addAppender(mockAppender)

    when(mockAppender.getName).thenReturn("MockAppender")

    slogger.info(CustomerCreatedEvent(when = ZonedDateTime.now, name = "Tigger", Tier.PREFERRED))

    val matchesExpectedOutput = new ArgumentMatcher[ILoggingEvent] {
      val ExpectedLogMessageRegEx = ".*customer created on .*, name: Tigger, tier: preferred"
      override def matches(arg: ILoggingEvent): Boolean = arg.getFormattedMessage.matches(ExpectedLogMessageRegEx)
    }

    verify(mockAppender).doAppend(argThat(matchesExpectedOutput))
  }

  "logger" should "not evaluate expression when log level is not enabled" in {
    //Trace is not enabled and so it should not fail...
    slogger.trace(fail)
  }

}
