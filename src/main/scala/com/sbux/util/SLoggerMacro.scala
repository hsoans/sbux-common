package com.sbux.util

import scala.reflect.macros.blackbox

private object SLoggerMacro {

  type LoggerContext = blackbox.Context {type PrefixType = SLogger}

  // Error

  def errorMessage[T](c: LoggerContext)(event: c.Expr[T]) = {
    import c.universe._
    val underlying = q"${c.prefix}.underlying"
    q"if ($underlying.isErrorEnabled) $underlying.error($event.toString)"
  }

  // Warn

  def warnMessage[T](c: LoggerContext)(event: c.Expr[T]) = {
    import c.universe._
    val underlying = q"${c.prefix}.underlying"
    q"if ($underlying.isWarnEnabled) $underlying.warn($event.toString)"
  }

  // Info

  def infoMessage[T](c: LoggerContext)(event: c.Expr[T]) = {
    import c.universe._
    val underlying = q"${c.prefix}.underlying"
    q"if ($underlying.isInfoEnabled) $underlying.info($event.toString)"
  }

  // Debug

  def debugMessage[T](c: LoggerContext)(event: c.Expr[T]) = {
    import c.universe._
    val underlying = q"${c.prefix}.underlying"
    q"if ($underlying.isDebugEnabled) $underlying.debug($event.toString)"
  }

  // Trace

  def traceMessage[T](c: LoggerContext)(event: c.Expr[T]) = {
    import c.universe._
    val underlying = q"${c.prefix}.underlying"
    q"if ($underlying.isTraceEnabled) $underlying.trace($event.toString)"
  }


}