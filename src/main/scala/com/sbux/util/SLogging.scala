package com.sbux.util

import org.slf4j.LoggerFactory

/**
  * Defines `logger` as a value initialized with an underlying `org.slf4j.Logger`
  * named according to the class into which this trait is mixed.
  */
trait SLogging {

  protected val slogger: SLogger =
    SLogger(LoggerFactory.getLogger(getClass.getName))
}
