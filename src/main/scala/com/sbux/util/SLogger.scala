package com.sbux.util

import scala.language.experimental.macros
import org.slf4j.{LoggerFactory, Marker, Logger => Underlying}

/**
  * Companion for [[SLogger]], providing a factory for [[SLogger]]s.
  */
object SLogger {

  /**
    * Create a [[SLogger]] wrapping the given underlying `org.slf4j.Logger`.
    */
  def apply(underlying: Underlying): SLogger =
    new SLogger(underlying)

  def apply(clazz: Class[_]): SLogger =
    new SLogger(LoggerFactory.getLogger(clazz.getName))

  def apply(name: String): SLogger =
    new SLogger(LoggerFactory.getLogger(name))
}

/**
  * Semantic Logger that logs "meaningful" log events, instead of just textual messages.
  * See semantic-logging.md for details.
  *
  * PRE-CONDITIONS
  *   event is a "meaningful" log event
  *     1. event.toString gives a readable text representation
  *     2. event should (de)serialize into correct json using jackson object mapper
  *
  * scala-logging is used under the terms of Apache 2.0 license.
  * @see com.typesafe.scalalogging.Logger
  */
final class SLogger private (val underlying: Underlying) extends Serializable {

  // Error

  def error[T](event: T): Unit = macro SLoggerMacro.errorMessage[T]

  // Warn

  def warn[T](event: T): Unit = macro SLoggerMacro.warnMessage[T]

  // Info

  def info[T](event: T): Unit = macro SLoggerMacro.infoMessage[T]

  // Debug

  def debug[T](event: T): Unit = macro SLoggerMacro.debugMessage[T]

  // Trace

  def trace[T](event: T): Unit = macro SLoggerMacro.traceMessage[T]
}